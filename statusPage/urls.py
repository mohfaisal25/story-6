from django.urls import path
from .views import add_status, index
#url for app
urlpatterns = [
    path('', index, name='index'),
    path('add_status/', add_status, name='add_status'),
]