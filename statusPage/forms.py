from django import forms

class StatusForm(forms.Form):
    attribute = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Write your status here...'
    }

    status = forms.CharField(label='', max_length=300, widget = forms.Textarea(attrs = attribute))