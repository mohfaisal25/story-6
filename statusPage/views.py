from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

# Create your views here.
response = {}
def index(request):
    response['status_form'] = StatusForm
    response['status_data'] = Status.objects.all()
    return render(request, 'index.html', response)

def add_status(request):
    form = StatusForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
