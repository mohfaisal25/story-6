# Generated by Django 2.1.1 on 2018-11-01 03:27

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('statusPage', '0003_auto_20181101_1016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 1, 3, 27, 10, 984249, tzinfo=utc)),
        ),
    ]
