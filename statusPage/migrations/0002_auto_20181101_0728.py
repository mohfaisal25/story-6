# Generated by Django 2.1.1 on 2018-11-01 00:28

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('statusPage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 11, 1, 0, 28, 56, 142209, tzinfo=utc)),
        ),
    ]
